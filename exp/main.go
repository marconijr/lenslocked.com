package main

import (
	"fmt"

	"lenslocked.com/models"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "lenslocked_dev"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	us, err := models.NewUserService(psqlInfo)
	if err != nil {
		panic(err)
	}
	defer us.Close()
	us.DestructiveReset()

	user := models.User{
		Name:     "Marconi Moreto Jr.",
		Email:    "me@marconijr.com",
		Password: "secret",
		Remember: "secret-remember",
	}
	if err = us.Create(&user); err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", user)

	user2, err := us.ByRemember("secret-remember")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", user2)
}
