package models

import (
	"fmt"
	"testing"
	"time"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "lenslocked_test"
)

func testingUserService() (*UserService, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	us, err := NewUserService(psqlInfo)
	if err != nil {
		panic(err)
	}
	// defer us.Close()
	us.db.LogMode(false)

	// Clear the users table between tests
	us.DestructiveReset()

	return us, nil
}

func TestCreateUser(t *testing.T) {
	us, err := testingUserService()
	if err != nil {
		t.Fatal(err)
	}

	user := &User{
		Name:  "Michael Scott",
		Email: "michael@dundermifflin.com",
	}
	if err = us.Create(user); err != nil {
		t.Fatal(err)
	}

	if user.ID == 0 {
		t.Errorf("Expected valid ID, received %d", user.ID)
	}
	if time.Since(user.CreatedAt) > time.Duration(5*time.Second) {
		t.Errorf("Expected CreatedAt to be recent. Received %s", user.CreatedAt)
	}
	if time.Since(user.UpdatedAt) > time.Duration(5*time.Second) {
		t.Errorf("Expected UpdatedAt to be recent. Received %s", user.UpdatedAt)
	}
}
